/*$("#menu_").click(function (){
  if($(this).hasClass('menu_close')){
    $('#nav').show();
    $('.user').show();
    $('#menu_').addClass('menu_close_style');
    $('#menu_').removeClass('menu_close');
  }
  else{
    $('#nav').hide();
    $('.user').hide();
    $('#menu_').removeClass('menu_close_style');
    $('#menu_').addClass('menu_close');
  }
});
if ($(window).width() < 740) {
  $("#nav").click(function (){
    $('#nav').hide();
    $('.user').hide();
    $('#menu_').removeClass('menu_close_style');
    $('#menu_').addClass('menu_close');
  });
}
else{}*/

$('#file').live('change', function (){
    //alert();
    $('#muncul').empty();
    for (var i = 0; i < this.files.length; i++)
    {
        $('#muncul').append(this.files[i].name).append("");
    }
});
$( ".menu_show" ).click(function() {
  $(this).find(".submenu" ).toggle(0);
});

$(document).ready(function() {
    $('#table_sort').DataTable( {
      "order": [[ 0, "desc" ]]
    } );
} );

/*S:LIQUID IMAGE*/
//liquid image
$(document).ready(function() {
    $(".lqd").imgLiquid();
});
/*E:LIQUID IMAGE*/

/*S:MODALBOX*/
if( $('div.pop_box').attr('id') == 'pop_box_now'){
    $("body").css('overflow','hidden');
}
else {
  $("body").css('overflow','scroll');
}

$(".box_modal").click(function (){
  if( $('div').attr('id') == 'pop_box_now'){}
  else{ 
    var src  = $(this).attr("alt");
    size   = src.split('|');
        url      = size[0],
        width    = size[1],
        height   = size[2],
        tops   = 'calc(50% - '+ (height/2) +'px)';
        tops2  = '-webkit-calc(50% - '+ (height/2) +'px)';

    $("body").append( "<div class='pop_box' id='pop_box_now'><iframe frameborder='0' id='framebox' src=''></iframe></div>" );
    $("#framebox").animate({
      height: height,
      width: width,
    },0).attr('src',url).css('top',tops).css('top',tops2);
    $("body").css('overflow','hidden');
  }
});

$(window).load(function(){
  $(".box_modal_full").click(function (){
    if( $('div').attr('id') == 'pop_box_now'){}
    else{ 
      $(this).removeAttr('href');
      var src  = $(this).attr("alt");
      size   = src.split('|');
      url      = size[0],
      width    = '100%',
      height   = '100%'
  
      $("body").append( "<div class='pop_box' id='pop_box_now'><iframe frameborder='0' id='framebox' src=''></iframe></div>" );
      $("#framebox").animate({
        height: height,
        width: width,
      },0).attr('src',url).css('position','fixed').css('top','0').css('left','0');
      $("body").css('overflow','hidden');
    }
    rescale();
  });
  $(function() {
    $(".pop_container").wrapInner( "<div id='pop_wrap'></div>" );
    $('#pop_wrap').css('height',$(window).height());
    $('#pop_wrap').css('width',$('#pop_wrap').parent('.pop_container').width());
  });
  function rescale(){
    var size = {width: $(window).width() , height: $(window).height() }
    $('#pop_wrap').css('height', size.height );
  }
  $(window).bind("resize", rescale);
  $(".box_modal2").click(function (){
    $("#pop_box2").show();
    $("body").css('overflow','hidden');
  });
  $(".box_modal3").click(function (){
    $("#pop_box3").show();
    $("body").css('overflow','hidden');
  });
  $(".box_modal_full2").click(function (){
    $(this).removeAttr('href');
    var src  = $(this).attr("alt");
     $.get(src, function (data) {
        $("#pop_box_full2").append(data);
        $(".close_box2").click(function (){
          closepop();
        });
        $(".box_modal_full3").click(function (){
          $("#pop_box_full2 .pop_box_content").remove();
          $(this).removeAttr('href');
          var src  = $(this).attr("alt");
          $.get(src, function (data) {
              $("#pop_box_full2").append(data);
              $(".close_box2").click(function (){
                closepop();
              });
          });
        });
    });
     $("#pop_box_full2").show();
    $("body").css('overflow','hidden');
  });
  

  function pop_next(src){
    size   = src.split('|');
    url      = size[0],
    width    = size[1],
    height   = size[2],
    tops   = 'calc(50% - '+ (height/2) +'px)';
    tops2  = '-webkit-calc(50% - '+ (height/2) +'px)';
    $("#framebox").animate({
      height: height,
      width: width,
    },0).attr('src',url).css('top',tops).css('top',tops2);
  };
});

function closepop()
{ 
  $("#pop_box_now").remove();
  $("#pop_box2").hide();
  $("#pop_box3").hide();
  $("#pop_box_full2").hide();
  $("#pop_box_full2 .pop_box_content").remove();
  $("body").css('overflow','scroll');
};
$(".close_box").click(function (){
  closepop();
});
$(".popbox_bg_close").click(function (){
  closepop();
});
$(".close_box_in").click(function (){
  parent.closepop();
});
$(".pop_next").click(function (){
  var src = $(this).attr("alt");
  parent.pop_next(src);
});
/*e:MODALBOX*/

/*s:tab*/
$(document).ready(function() {
  $('#filterOptions li a').click(function() {
    // fetch the class of the clicked item
    var ourClass = $(this).attr('class');
    
    // reset the active class on all the buttons
    $('#filterOptions li').removeClass('active');
    // update the active state on our clicked button
    $(this).parent().addClass('active');
    
    if(ourClass == 'all') {
      // show all our items
      $('#ourHolder').children('div.item').show();  
    }
    else {
      // hide all elements that don't share ourClass
      $('#ourHolder').children('div:not(.' + ourClass + ')').hide();
      // show all elements that do share ourClass
      $('#ourHolder').children('div.' + ourClass).show();
    }
    return false;
  });
});
/*e:tab*/



/*S:DATEPICKER*/
$(function() {
  
  $('.datepick').datepick({
    dateFormat: $.datepick.ISO_8601,
    yearRange: '1900:2017', 
    showTrigger: '#calImg',
    dateFormat: 'dd/m/yyyy', 
  });
  
  /*SAMPLE DATE SELECTED*/
  var SelectedDates = {};
    SelectedDates[new Date('04/05/2015')] = new Date('04/05/2015');
    SelectedDates[new Date('05/04/2015')] = new Date('05/04/2015');
    SelectedDates[new Date('06/06/2015')] = new Date('06/06/2015');
  
  var SelectedText = {};
    SelectedText[new Date('04/05/2015')] = 'Holiday1';
    SelectedText[new Date('05/04/2015')] = 'Holiday2';
    SelectedText[new Date('06/06/2015')] = 'Holiday3';  
  
  $('#multiInlinePicker').datepick({ 
    multiSelect: 999,
    prevText: 'Prev', nextText: 'Next',
    dateFormat: 'D, M dd, yyyy', 
    altField: '#altOutput',
    multiSeparator: ' -- ',
    onSelect: function(dateText, inst){
      var hiddenDate = $("[name='altInput']").val();
      $("#myDate").html(hiddenDate);
      },
    beforeShowDay: function(dateText, inst) {
            var Highlight = SelectedDates[dateText];
            var HighlighText = SelectedText[dateText]; 
            if (Highlight) {
                return [true, "Highlighted", HighlighText];
            }
            else {
                return [true, '', ''];
            }
        },
    onDate: function(date, current) { 
      return !current ? {} : 
        {
        /*content: date.getDate() + '<br><sub>' + 
        $.datepick.dayOfYear(date) + '</sub>', */
        dateClass: 'showDoY',
        selectable: true,
        title: 'Event Name'
        }; 
    },
    onShow: $.datepick.hoverCallback(showHover)
    /*onShow: $.datepick.multipleEvents( 
        $.datepick.selectWeek, $.datepick.showStatus)*/
  });
  
  function showHover(date, selectable) { 
    $("#showDate").html(!selectable ? "" :
      $.datepick.formatDate("D, M dd, yyyy", date) + '<br>');

  }
  
  /*$('#setDates').click(function() {
      var dates = $('#altOutput').val().split(',');
    var size = $(dates).size();
    for(x = 0; x < size; x++) {
      $("#myDates").append("<a href=''>" + dates[x] + "</a><br>");
    }
    x++;
  });*/
  
});

function showDate(date) {
  alert('The date chosen is ' + date);
}

/*E:DATEPICKER*/

/*tinymce.init({
  selector: '.tinymc',
  height: 300,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css']
});*/
tinymce.init({
  selector: ".tinymc",
  //images_upload_url: 'uploads',
  images_upload_url: 'http://localhost/dana/postAcceptor.php',
  automatic_uploads: true,
  height: 300,
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools'
  ],
  toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });



function reset () {
    alertify.set({
        labels : {
            ok     : "OK",
            cancel : "Cancel"
        },
        delay : 5000,
        buttonReverse : false,
        buttonFocus   : "ok"
    });
}
function deletes () {
    alertify.set({
        labels : {
            ok     : "Yes",
            cancel : "No"
        },
        delay : 5000,
        buttonReverse : false,
        buttonFocus   : "ok"
    });
}

function respon () {
    alertify.set({
        labels : {
            ok     : "Yes",
            cancel : "No"
        },
        delay : 5000,
        buttonReverse : false,
        buttonFocus   : "ok"
    });
}

$(".alert").on( 'click', function () {
    reset();
    alertify.alert("This is an alert dialog");
    return false;
});
$(".delete").on( 'click', function () {
    var target  = $(this).attr("href");
    deletes();
    alertify.confirm("Are you sure to delete this", function (e) {
        if (e) {
            window.location = target;
            //alert("ok");
        } else {
            // user clicked "cancel"
            //alert("cancel");
        }
    });
    //alertify.confirm("Are you sure to delete this", function (e) {});
    return false;
});
$(".respon").on( 'click', function () {
    var target  = $(this).attr("href");
    respon();
    alertify.confirm("Email ini sudah direspon", function (e) {
        if (e) {
            window.location = target;
            //alert("ok");
        } else {
            // user clicked "cancel"
            //alert("cancel");
        }
    });
    //alertify.confirm("Are you sure to delete this", function (e) {});
    return false;
});



