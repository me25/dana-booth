<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prize extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "Prize";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('prize', array());
		$result = $query->result_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);
		$win = array (
			'0' => 'Win', 
			'1' => 'Not Win'
		);

		$data = array(
			'content' => 'prize/index',
			'result' => $result,
			'status' => $status,
			'win' => $win,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function detail()
	{
		$page = "Merchant";
		$id = $this->uri->segment(3);
		$namaalbum = $this->uri->segment(5);

		$page_query = $this->Modglobal->find('merchant', array('id' => $id));
		$page_detail = $page_query->row_array();

		$list_query= $this->Modglobal->find('merchant_list', array('merchant_id' => $id));
		$list = $list_query->result_array();
		$num = $list_query->num_rows();

		$kota_query= $this->Modglobal->find('kota', array());
		$kota = $kota_query->result_array();


		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$win = array (
			'0' => 'Win', 
			'1' => 'Not Win'
		);

		

		$data = array(
			'content' => 'merchant/index_store',
			'page' => $page,
			'page_detail' => $page_detail,
			'list' => $list,
			'id' => $id,
			'num' => $num,
			'status' => $status,
			'win' => $win,
			'kota' => $kota,
		);
		$this->load->view('layouts/base', $data);
	}
	public function add()
	{
		$page = "Prize";
		$form = "add";
		$user_id = $this->session->userdata('id');


		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);
		$win = array (
			'0' => 'Win', 
			'1' => 'Not Win'
		);

		$data = array(
			'content' => 'prize/form',
			'page' => $page,
			'form' => $form,
			'status' => $status,
			'win' => $win,
		);
		$this->load->view('layouts/base', $data);
	}
	public function add_pro()
	{
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'prize-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../media/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }

		$data = array(
        	'nama' => $this->input->post('nama'),
        	'status' => $this->input->post('status'),
        	'win' => $this->input->post('win'),
        	'img' => $img,
        );
        $this->Modglobal->insert('prize', $data);
		redirect('prize');
	}
	public function edit()
	{
		$page = "Prize";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('prize', array('id' => $id));
		$page_detail = $query->row_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);
		$win = array (
			'0' => 'Win', 
			'1' => 'Not Win'
		);

		$data = array(
			'content' => 'prize/form',
			'page' => $page,
			'page_detail' => $page_detail,
			'id' => $id,
			'status' => $status,
			'win' => $win,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function update(){
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'prize-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../media/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }
	    
		$data = array(
			'nama' => $this->input->post('nama'),
        	'status' => $this->input->post('status'),
        	'win' => $this->input->post('win'),
        	'img' => $img,
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('prize', $data, $where);
		redirect('prize');
	}
	public function delete() {
		$id = $this->uri->segment(3);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('prize', $where);
		//echo $id;

		redirect('prize');
	}
	
}


