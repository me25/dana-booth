<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');

		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "User";
		$user_id = $this->session->userdata('id');

		$user_query= $this->Modglobal->find('data_register', array());
		$user = $user_query->result_array();
		$user_num = $user_query->num_rows();

		$status = array (
			'0' => 'Not Play', 
			'1' => 'Play'
		);


		$data = array(
			'content' => 'user/index',
			'user' => $user,
			'user_num' => $user_num,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function delete() {
		$this->db->empty_table('data_register');

		redirect('user/');
	}
	
}


