<header>
	<a class="logo">
		<img src="<?php echo assets_url('images');?>/logo_dana.png" alt="">
	</a>
	<nav>
		<a href="<?php echo base_url('home');?>" class="<?php if($page=="dashboard") {echo 'selected';}?>">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_dashboard.png" alt="">
			</span>
			<span class="menu">Dashboard</span>
		</a>
		<a href="<?php echo base_url('user');?>" class="<?php if($page=="User") {echo 'selected';}?>">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Data User</span>
		</a>
		<a href="<?php echo base_url('prize');?>" class="<?php if($page=="Prize") {echo 'selected';}?>">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Prize</span>
		</a>
		<a href="<?php echo base_url('rule');?>" class="<?php if($page=="Rule") {echo 'selected';}?>">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Prize Rules</span>
		</a>
		<!-- <div class="menu_show <?php if($page=="Rule") {echo 'selected';}?>">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Prize Rules</span>
			<div class="submenu">
				<a href="<?php echo base_url('rule');?>">Set Number</a>
				<a href="<?php echo base_url('rule/set');?>">Set Rules</a>
			</div>
		</div> -->
		<div href="#" class="<?php if($page=="Statik"){ echo 'selected';}?> menu_show">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Statik Page</span>
			<div class="submenu">
				<a href="<?php echo base_url('statik/home');?>">Home</a>
				<a href="<?php echo base_url('statik/success');?>">Success Page</a>
				<a href="<?php echo base_url('statik/fail');?>">Fail Page</a>
				<a href="<?php echo base_url('statik/giftbox');?>">Giftbox Page</a>
				<a href="<?php echo base_url('statik/winpage');?>">Prize Page</a>
				<a href="<?php echo base_url('statik/notwin');?>">Not Win Page</a>
				<a href="<?php echo base_url('statik/done');?>">Done Page</a>
			</div>
		</div>

		<a href="<?php echo base_url('member');?>" class="<?php if($page=="member") {echo 'selected';}?>">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Admin</span>
		</a>
	</nav>
</header>
