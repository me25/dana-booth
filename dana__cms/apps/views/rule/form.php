
<div class="content_ful">
	<div class="table_show">
		<div class="table_head">
			<div class="info"><h2>Prize Rules</h2></div>
			<form action="<?php echo base_url('rule/importcsv/');?>" class="import fr" method="post" enctype="multipart/form-data">
				<span>Import CSV</span>
				<input type="file" name="file">
				<input name="campaign_id" type="hidden" value="<?php echo $page_detail['id'];?>">
				<input type="submit" value="Upload" class="btn_add">
			</form>
			<div class="clearfix"></div>
		</div>
<style>
	.form_posisi .select-style {
		float: left;
	}
	.form_posisi .hapus2 {
		margin-left: 20px;
		padding-top: 10px;
		display: inline-block;	
		color: red;
		font-weight: bold;
		cursor: pointer;
	}
	.form_posisi .hapus2:hover {
		text-decoration: underline;
	}
	.form_posisi .form-group:first-child .hapus2{
		display: none;
	}
	#tambah_rule .form-group:first-child .hapus2{
		display: inline-block;
	}
	#btn_tambah:hover {
		text-decoration: underline;
		cursor: pointer;
	}
</style>

		<hr color="#eee">
		<form action="<?php echo base_url('rule/update');?>" class="form_1 form_posisi" method="post"  enctype="multipart/form-data">
			<?php
			$no_urut = 0;
			foreach ($result as $row) {
				$no_urut++;
				?>
				<div class="form-group">
					<div style="width: 5%; float: left; margin-top: 10px;">
						<input type="hidden" name="id[]" value="<?php echo $row['id'];?>">
						<?php echo $no_urut;?>
					</div>
					<div style="width: 80%; float: left;">
				      	<div class="select-style">
							<span></span>
							<select name="prize_id[]" id="" required="">
								<?php
								foreach ($prize as $row_prize) {
									if($row['prize_id'] == $row_prize['id']){
										echo '<option value="'.$row_prize['id'].'" selected>'.$row_prize['nama'].'</option>';
									}
									else{
										echo '<option value="'.$row_prize['id'].'">'.$row_prize['nama'].'</option>';
									}
								}
							?>
							</select>
							
						</div>
						<span class="hapus2">Delete</span>

					</div>
				</div>

				<div class="clearfix"><br></div>
				<?php
			}
			?>

			<div class="clearfix"></div>
			<div id="tambah_rule"></div>
			<br>
			<a id="btn_tambah">Tambah Rule</a>
			<br>
			<br>
		    <div>
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
		<div style="display: none;">
			<div class="form-group duplicate">
				<div style="width: 5%; float: left;">
					<input type="hidden" name="id[]" value="1">
				</div>
				<div style="width: 80%; float: left;">
			      	<div class="select-style">
						<span></span>
						<select name="prize_id[]" id="" required="">
							<?php
							foreach ($prize as $row_prize) {
								echo '<option value="'.$row_prize['id'].'">'.$row_prize['nama'].'</option>';
							}
						?>
						</select>

					</div>
					<span class="hapus2">Delete</span>
				</div>
				<div class="clearfix"><br></div>
			</div>
		</div>
	</div>
</div>

<script>
	$(function() {
	    $("#btn_tambah").click(function () {
		  //$("#tambah_rule").append('<div class="module_holder"><div class="module_item"><img src="images/i-5.png" alt="Sweep Stakes"><br>sendSMS</div></div>');
		  $( ".duplicate" ).clone().prependTo( "#tambah_rule" );

		  $( "#tambah_rule" ).find('.duplicate').removeClass('duplicate');

		  $(".hapus2").click(function () {
		  	$(this).parent().parent().remove();
		  });


		});
		$(".hapus2").click(function () {
		  	$(this).parent().parent().remove();
		  });
	});
</script>





