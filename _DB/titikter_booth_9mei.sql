-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 09 Bulan Mei 2019 pada 10.40
-- Versi server: 5.6.43
-- Versi PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `titikter_booth`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_register`
--

CREATE TABLE `data_register` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `hadiah` int(11) DEFAULT '0',
  `status` varchar(255) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_register`
--

INSERT INTO `data_register` (`id`, `nama`, `email`, `phone`, `hadiah`, `status`) VALUES
(1, 'tes2', NULL, '081905458080', 1, '0'),
(2, 'kolim', NULL, '084561717171', 3, '0'),
(3, 'Afina Rezkiani', NULL, '081316881611', 3, '0'),
(4, 'Afina', NULL, '0813168', 1, '0'),
(5, 'Reza', NULL, '08112010406', 1, '0'),
(6, 'zaza', NULL, '234556781234', 3, '0'),
(7, 'zaza', NULL, '234556781234', 3, '0'),
(8, 'kelik', NULL, '082345781902', 1, '0'),
(9, 'sdafasdf', NULL, '00999', 3, '0'),
(10, 'Shella Fergiana', NULL, '081298084991', 1, '0'),
(11, 'Shella', NULL, '081230794577', 3, '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `prize`
--

CREATE TABLE `prize` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `win` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `prize`
--

INSERT INTO `prize` (`id`, `nama`, `img`, `status`, `win`) VALUES
(1, 'Voucher', 'prize-1556422937voucher.png', 0, 0),
(2, 'Iphone X', 'prize-1556422926iphonex.png', 0, 0),
(3, 'Not Win', 'prize-1556423005unlucky.png', 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rule`
--

CREATE TABLE `rule` (
  `id` int(11) UNSIGNED NOT NULL,
  `prize_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `delete_id` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rule`
--

INSERT INTO `rule` (`id`, `prize_id`, `status`, `delete_id`) VALUES
(145, 1, 1, 0),
(146, 3, 1, 0),
(147, 1, 1, 0),
(148, 3, 1, 0),
(149, 1, 1, 0),
(150, 3, 1, 0),
(151, 1, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `statik`
--

CREATE TABLE `statik` (
  `id` int(11) UNSIGNED NOT NULL,
  `page` varchar(50) DEFAULT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `desc` text,
  `date_edit` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `statik`
--

INSERT INTO `statik` (`id`, `page`, `judul`, `desc`, `date_edit`) VALUES
(1, 'home', 'Ayo Isi Data Diri dan Menangkan Hadiahnya&#33;', '', '2019/05/02 - 01:30:11pm'),
(2, 'success page', 'Ayo scan Kode QR & dapatkan hadiahnya&#33;', '', '2019/05/02 - 01:30:26pm'),
(3, 'Fail page', 'Hai, nomor kamu sudah terdaftar.', '', '2019/05/02 - 01:34:24pm'),
(4, 'game', 'Hadiah kamu sudah menanti. ', '<p>Tap kadonya sekarang&#33;</p>', '2019/05/02 - 01:44:49pm'),
(5, 'prize', 'Selamat&#33;', '<p>Kamu mendapatkan</p>', '2019/05/02 - 10:15:53pm'),
(6, 'done', 'Hai, kamu sudah pernah bermain.', '', '2019/05/08 - 08:51:44am'),
(7, 'unlucky', 'Maaf kamu belum beruntung,', '<p>Coba lagi lain waktu ya&#33;</p>', '2019/05/08 - 08:49:45am'),
(8, 'rule', NULL, '17', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT 'tes',
  `level` varchar(11) NOT NULL DEFAULT '0',
  `email` varchar(100) NOT NULL,
  `status` int(1) NOT NULL,
  `last_login` varchar(33) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`, `email`, `status`, `last_login`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '1', 'admin', 0, ''),
(131, 'dani', 'e94d51a35484755a9f9672d13687f499', '0', 'me@danirus.com', 0, '');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data_register`
--
ALTER TABLE `data_register`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `prize`
--
ALTER TABLE `prize`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `rule`
--
ALTER TABLE `rule`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `statik`
--
ALTER TABLE `statik`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data_register`
--
ALTER TABLE `data_register`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `prize`
--
ALTER TABLE `prize`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `rule`
--
ALTER TABLE `rule`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;

--
-- AUTO_INCREMENT untuk tabel `statik`
--
ALTER TABLE `statik`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
