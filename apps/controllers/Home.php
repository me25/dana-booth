<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


	}
	public function index()
	{
		$this->session->sess_destroy();

		$page = "home";


		$detail_page_query= $this->Modglobal->find('statik', array('id' => '1'));
		$detail_page = $detail_page_query->row_array();

		$data = array(
			'content' => 'index',
			'page' => $page,
			'detail_page' => $detail_page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function daftar()
	{
		$phone = $this->input->post('phone');
		$nama =  str_replace_text($this->input->post('nama'));

		$checkvalid = $this->Modglobal->find('data_register', array('phone' => $phone));
		$checkvalid_num = $checkvalid->num_rows();

		if($checkvalid_num > 0) {
			redirect('home/fail');
		}
		else{

			$prize_query= $this->Modglobal->find('rule', array('status' => '0'));
			$prize = $prize_query->row_array();
			$prize_num = $prize_query->num_rows();

	        if($prize_num > 0) {
		    }
		    else {
		    	$data = array(
		        	'status' => '0',
		        );
		        $where = array(
		    		'status' => '1',
		        );
		        $this->Modglobal->update('rule', $data, $where);
		        $prize_query= $this->Modglobal->find('rule', array('status' => '0'));
				$prize = $prize_query->row_array();
		    }

		    if(is_numeric($this->input->post('phone'))) {
		    	$data = array(
		        	'status' => '1',
		        );
		        $where = array(
		    		'id' => $prize['id'],
		        );
		        $this->Modglobal->update('rule', $data, $where);

		        $data = array(
		        	'nama' => str_replace_text($this->input->post('nama')),
		        	'phone' => $this->input->post('phone'),
		        	'hadiah' => $prize['prize_id'],
		        );
		        $this->Modglobal->insert('data_register', $data);

		        $session_data = array(
					'nama' => $this->input->post('nama'),
					'phone' => $this->input->post('phone'),
					'logged_in' => 1
				);
				$this->session->sess_expiration = '72800';
				$this->session->set_userdata($session_data);

		        redirect('home/success');
		    }
		    redirect('home');

		    
		}
		
	}
	public function success()
	{
		if($this->session->userdata('logged_in')){
			$page = "home";

			$detail_page_query= $this->Modglobal->find('statik', array('id' => '2'));
			$detail_page = $detail_page_query->row_array();

			$data = array(
				'content' => 'success',
				'page' => $page,
				'detail_page' => $detail_page,
			);
			$this->load->view('layouts/base', $data);
		}
		else{
			redirect('home');
		}
	}
	public function fail()
	{
		$page = "home";

		$detail_page_query= $this->Modglobal->find('statik', array('id' => '3'));
		$detail_page = $detail_page_query->row_array();

		$data = array(
			'content' => 'fail',
			'page' => $page,
			'detail_page' => $detail_page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function scan()
	{
		$phone = $this->uri->segment(3);
		//$nama = $this->uri->segment(4);
		//echo $phone;

		//$phone = $this->input->post('phone');
		//$nama =  str_replace_text($this->input->post('nama'));

		$checkvalid = $this->Modglobal->find('data_register', array('phone' => $phone));
		$detail_page = $checkvalid->row_array();
		$checkvalid_num = $checkvalid->num_rows();

		if($checkvalid_num > 0) {
			$session_data = array(
				'nama' => $detail_page['nama'],
				'email' => $detail_page['email'],
				'phone' => $detail_page['phone'],
				'id' => $detail_page['id'],
				'hadiah' => $detail_page['hadiah'],
				'status' => $detail_page['status'],
				'logged_in' => 1
			);
			$this->session->sess_expiration = '72800';
			$this->session->set_userdata($session_data);

			if($this->session->userdata('status') == '0'){
				redirect('home/giftbox');
				//echo 'boleh';
				//echo $this->session->userdata('status');
			}
			else {
				redirect('home/done');
				//echo 'udah';
				//echo $this->session->userdata('status');
			}

		}
		else{
			redirect('home/none');
		}
	}
	public function giftbox()
	{
		if($this->session->userdata('logged_in')){
			$page = "home";

			$detail_page_query= $this->Modglobal->find('statik', array('id' => '4'));
			$detail_page = $detail_page_query->row_array();

			$detail_page2_query= $this->Modglobal->find('statik', array('id' => '5'));
			$detail_page2 = $detail_page2_query->row_array();

			$notwin_query= $this->Modglobal->find('statik', array('id' => '7'));
			$notwin = $notwin_query->row_array();

			$data = array(
				'content' => 'giftbox',
				'page' => $page,
				'detail_page' => $detail_page,
				'detail_page2' => $detail_page2,
				'notwin' => $notwin,
			);
			$this->load->view('layouts/mobile', $data);
		}
		else{
			redirect('home/none');
		}
	}
	
	
	
	
}


