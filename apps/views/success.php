<a href="<?php echo base_url();?>" class="logo center"><img src="<?php echo assets_url('img'); ?>/logo.png" alt=""></a>
<div class="qr_area">
	<h2>
		Hai <?php echo $this->session->userdata('nama');?>,<br>
		<?php echo $detail_page['judul'];?>
	</h2>
	<?php echo $detail_page['desc'];?>


	
	<br><br><br>
	<?php $this->load->view('vendor/qrcode/qrlib.php')?>
	<?php
	 
	$tempdir = "qr/"; //<-- Nama Folder file QR Code kita nantinya akan disimpan
	if (!file_exists($tempdir))#kalau folder belum ada, maka buat.
	    mkdir($tempdir);


	#parameter inputan
	$isi_teks = base_url('home/scan/').$this->session->userdata('phone');
	$namafile = $this->session->userdata('phone').'.png';
	$quality = 'H'; //ada 4 pilihan, L (Low), M(Medium), Q(Good), H(High)
	$ukuran = 20; //batasan 1 paling kecil, 10 paling besar
	$padding = 0;
	 
	QRCode::png($isi_teks,$tempdir.$namafile,$quality,$ukuran,$padding);


	 
	?>

	<img src="<?php echo base_url('qr/').$namafile;?>" alt="" class="qr_img">

</div>