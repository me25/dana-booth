<!doctype html>	
<html>
<head>
	<?php $this->load->view('includes/head')?>	
</head>
<body>
<?php
if (isset($content) && !empty($content)) {
  $this->load->view($content);
}
?>
<div class="clearfix"></div>
<?php $this->load->view('includes/js')?>
</body>
</html>