<div id="header" class="<?php echo $header;?>">
	<div class="container">
		<a href="<?php echo base_url('home'); ?>" class="logo"><img src="<?php echo assets_url('img'); ?>/logo.png" alt=""></a>
		<div id="nav">
			<a href="<?php echo base_url('produk'); ?>" class="<?php if($page=="produk") {echo 'selected';}?>">Produk & Fitur</a>
			<a href="<?php echo base_url('bisnis'); ?>" class="<?php if($page=="bisnis") {echo 'selected';}?>">Bisnis & Partner</a>
			<a href="<?php echo base_url('karir'); ?>" class="<?php if($page=="karir") {echo 'selected';}?>">Karir</a>
			<a href="<?php echo base_url('promo'); ?>" class="<?php if($page=="promo") {echo 'selected';}?>">Promo</a>
			<a href="<?php echo base_url('faq'); ?>" class="<?php if($page=="faq") {echo 'selected';}?>">FAQ</a>
			<a href="<?php echo base_url('download'); ?>" class="btn_download">Download</a>
		</div>
		<div class="clearfix"></div>
	</div>
	<div id="menu_" class="menu_show menu_close">
		<span></span>
		<span></span>
		<span></span>
	</div>
</div>
